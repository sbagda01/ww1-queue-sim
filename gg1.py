"""
    A waiting system simulation of Weibull/Weibull/SERVER_CAP.
    This implementation used a simulation library simpy to delevop a scalable
    and more realistic simulator. 
"""

import simpy 
import numpy as np
from scipy.stats import weibull_min, variation
from scipy.special import  gamma
import matplotlib.pyplot as plt
from  numpy import sum
from array import array
import random
import sys
import click

#some default values used globally 
ARR_ALPHA=2                 #arrival shape   
ARR_BETA=72                 #arrival scale
SER_ALPHA=0.75              #service shape
SER_BETA=22                 #service scale
BETAS=[21.298,37.28,45.26,49.25] #list of betas to tweak the loading factor  
CONF_ITER=10                #how many times to repeat the simulation


#Time lists for further stats
#I am using integer arrays instead lists, this
#speeds up the time dramatically
service_times=array('f')
arrival_times=array('f')
waitingq_times=array('f')
waiting_times=array('f')
lovert=[]

def log(s):
    if(LOGGING=="ON"):
        print(s,file=LOGFILE)

def arrivals(env,server,b):
    """Generates the arrivals randomly given by Weibull distribution."""
    
    log("Client generation process started")
    CLIENT_NUMBER=0

    bar_out=sys.stdout
    if(SIM=='F'):
        bar_out=open("/dev/null",'w')

    #ignore this line it used for the progress bar
    with click.progressbar(range(CLIENTS),file=bar_out) as CLIENTSb:
        #here the arrival times are generated 
        for CLIENT_NUMBER in CLIENTSb:
            
            #after elapsed a new arrival is generated
            elapsed=random.weibullvariate(ARR_BETA,ARR_ALPHA);
            #wait for weibull random time to spawn the next client
            yield env.timeout(elapsed)
            #the list with the  arrival times for later stats
            arrival_times.append(elapsed)

            #clients are modeled as processes, each client wants the 
            #resource server. Server only serves one client at a time.
            env.process(client(env,'Client_%d' % CLIENT_NUMBER,server,b))
            
            #collect a sample every 1 unit off time
            l_uptonow=obs_l(waiting_times,arrival_times)
            lovert.append((env.now,l_uptonow))

def service(env,name,b):
    """Generates service times"""
    
    #the service time is generated based on Weibull distr.
    elapsed=random.weibullvariate(b,SER_ALPHA);
    #the client just waits in the server emulating some work.
    yield env.timeout(elapsed)
    
    log("Service time for %s:%d" % (name,elapsed))
    
    #the list with the  arrival times for later stats
    service_times.append(elapsed)
    
def client(env,name,server,b):
    """New customer arrives to the server and waits in the queue if needed"""
    
    log('I am %s and now I request the server at %d' % (name,env.now))
    
    #keep the arrive time
    arrive=env.now
    
    #Request the server, this really serves as a FIFO queue.
    #This client waits another to free the server.
    req=server.request()
    yield req

    #at this point the client enters the server
    waitq=env.now-arrive
    waitingq_times.append(waitq)

    log('%s is in the server after waiting for %d' % (name,waitq))
        
    #This will generate the service time for this client,
    #and the client will just wait for that time.
    yield env.process(service(env,name,b)) 

    #After the client is finished the server is released.
    yield server.release(req)

    wait=env.now-arrive
    waiting_times.append(wait)

def unique(data):
    """How many unique numbers in a list"""
    return len(list(set(data)))

def plot_hist(stimes,name):
    """Plot a histogram with the stimes data list"""
    
    un=unique(stimes)  #number of unique numbers
    step=un/10+1
    bins = np.arange(un) - 0.5
    plt.hist(stimes,bins,histtype='stepfilled',color='r',alpha=0.5)
    plt.xticks(range(0,un,int(step)))
    plt.xlim([0, un])
   
    plt.xlabel(name)
    plt.ylabel("Frequency")
    name=name.replace(' ','')
    plt.savefig('%s.png'%name,dpi=600)
    print('Histogram %s.png saved.'%name)
    plt.close()

def cv(alpha,beta):
    """Compute the coefficient of variation of Weibull distr with this shape"""
    return  ex_std(alpha,beta)/ex_mean(alpha,beta)

def ex_lq(arr_alpha,arr_beta,ser_alpha,ser_beta):
    """Theoretical number of customers in waiting queue for Weibull\Weibull"""
    return ex_rate(arr_alpha,arr_beta)*ex_wq(arr_alpha,arr_beta,ser_alpha,ser_beta)

def ex_std(alpha,beta):
    return np.sqrt(ex_var(alpha,beta))

def ex_mean(alpha,beta):
    """Weibull theoretical mean """
    return beta*gamma((alpha+1)/alpha) 

def ex_var(a,beta):
    """Weibull theoretical variance """
    return np.square(beta)*(gamma((a+2)/a)-np.square(gamma((a+1)/a)))

def ex_rate(alpha,beta):
    """Weibull theoretical rate """
    return 1/ex_mean(alpha,beta)  

def ex_load_factor(arr_alpha,arr_beta,ser_alpha,ser_beta):
    """Weibull\Weibull theoretical load factor """
    return ex_rate(arr_alpha,arr_beta)/ex_rate(ser_alpha,ser_beta)

def ex_wq(arr_alpha,arr_beta,ser_alpha,ser_beta):
    """Theoretical queue wating per client for Lq\l """
    pe=ex_load_factor(arr_alpha,arr_beta,ser_alpha,ser_beta)
    cvs=np.square(cv(arr_alpha,arr_beta))+np.square(cv(ser_alpha,ser_beta)) 
    return (cvs*pe/(2*ex_rate(ser_alpha,ser_beta)*(1-pe)))

def obs_mean(data):
    if(len(data)==0):return 0
    return sum(data)/len(data)

def obs_rate(data):
    if(len(data)==0):return 0
    return 1/obs_mean(data)

def obs_load_factor(adata,sdata):
    return obs_rate(adata)/obs_rate(sdata) 

def obs_wq(wdata):
    return obs_mean(wdata)

def obs_lq(wdata,adata):
    return obs_wq(wdata)*obs_rate(adata)

def obs_w(wdata):
    return obs_mean(wdata)

def obs_l(wdata,adata):
    return obs_w(wdata)*obs_rate(adata)

def clean_lists():
    del arrival_times[:]
    del waiting_times[:]
    del waitingq_times[:]
    del lovert[:]
    del service_times[:]

def simulate(b):
    #set up the sim environment
    env=simpy.Environment();
    
    #we have a server with SERVER_CAP capacity (1 by default)
    server=simpy.Resource(env,capacity=SERVER_CAP)
       
    #fire up the client generation
    env.process(arrivals(env,server,b))
    env.run()

def pstats(s):
    print(s,file=STATS_FILE)

def pqstats(wqs,lqs,b):
    plt.xlabel("Time(discrete time units)")
    plt.ylabel("Average W.S Occupation(L)")
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0)) 
    plt.plot(*zip(*lovert),color='green',alpha=0.5)
    plt.savefig('Lb=%2.f.png'%b,dpi=600)
    plt.close()
    pstats("Queue Stats for service beta=%.3f and p=%.3f"%(b,
        ex_load_factor(ARR_ALPHA,ARR_BETA,SER_ALPHA,b)))
    pstats("\tExpected\tObserved",)
    pstats("Wq  \t%f\t%f"%(ex_wq(ARR_ALPHA,ARR_BETA,SER_ALPHA,b),
        (obs_mean(wqs))))
    pstats("Lq  \t%f\t%f"% (ex_lq(ARR_ALPHA,ARR_BETA,SER_ALPHA,b),
        (obs_mean(lqs))))

def run_full():
    #run a simulation for all beta
    for b in BETAS:
        
        #we need this lists to keep the wq,lq per simulation
        wqs=[]
        lqs=[]
        
        #be informative
        print('For service beta=%d perform %d iterations.'%(b,CONF_ITER)) 

        #progress bar for all beta simulations
        with click.progressbar(range(CONF_ITER)) as CONF_ITERb:
            
            #run multiple simulations with the same parameters to obtain conf.
            for i in CONF_ITERb:    
                
                #seed the RNG with date and time
                random.seed(None)
         
                #clean up before starting
                clean_lists()
        
                #run the simulation
                simulate(b)
               
                #save these values to obtain averages
                wqs.append(obs_wq(waitingq_times))
                lqs.append(obs_lq(waitingq_times,arrival_times))   
       
        #The simulations are over, lets print the stats
        pqstats(wqs,lqs,b);

def p_part_stats(data,name,alpha,beta):  
    """
        Computer and print Mean, Variance and CV of the data list 
        and Mean, Variance and CV of a Weibull with shape=alpha
        and scale=beta.
    """
    pstats(name)
    pstats("\tExpected\tObserved(S/C)")
    pstats('Mean\t%f\t%f' % (ex_mean(alpha,beta),obs_mean(data)))
    pstats("Vari\t%f\t%f" % (ex_var(alpha,beta),np.var(data)))
    pstats("Coef\t%f\t%f" % (cv(alpha,beta),variation(data)))        
       
def test_stimes():
    #run one time simulation to print stats
    simulate(SER_BETA)

    #print stats for arrival and service distrs.
    p_part_stats(arrival_times,"Arrival Stats",ARR_ALPHA,ARR_BETA)
    p_part_stats(service_times,"Service Stats",SER_ALPHA,SER_BETA)

    #This is  needed for the  histograms
    plot_hist(service_times,"Service Time")
    plot_hist(arrival_times,"Arrival Time")

def main():
    """Perform the simulation and compute the stats of a W/W/n"""

    print('Simulation of Weibull/Weibull/n queue with %d clients'% CLIENTS)
  
    pstats('Simulation initial parameters for Weibull/Weibull/n:')
    pstats('Arrival alpha=%.3f'%ARR_ALPHA)
    pstats('Arrival beta=%.3f'%ARR_BETA)
    pstats('Service alpha=%.3f'%SER_ALPHA)
    pstats('Service beta=%.3f'%SER_BETA)
    pstats('Clients number=%d'%CLIENTS)
    pstats('Server capacity=%d'%SERVER_CAP)
    if(SIM=='F'):
        pstats("##Running complete simulation##")
        run_full()
    elif(SIM=='T'):
        test_stimes()
     

#makes sure that if the module is imported
#main is not executed
if __name__ == '__main__': 
    try:
        CLIENTS  = int(sys.argv[1])
        SERVER_CAP = int(sys.argv[2])
        SIM=sys.argv[3]
        LOGGING=sys.argv[4]
        if(LOGGING=="ON"):
            LOGFILE=open('log.txt','w')
        STATS_FILE=open('stats.txt','w')
    except IndexError:
        print("Usage: myprogram.py <CLIENTN> <SERVERCAP> <F | T> <ON|OFF>")
        print("If F argument is given a full simulation is performed.")
        print("If T argument is given a service times generation is tested.")
        print("The ON or OFF parameter refers to the logging to log.txt")
        sys.exit(1)
    main()
