A waiting system simulation of Weibull/Weibull/SERVER_CAP. This programm was developed with python3.

Packages:
	simpy,numpy,scipy,matplotlibi,click

To install the packages open the terminal and run: 
	pip3 install simpy numpy scipy matplotlibi click

Execution: 
	type "python gg1.py" to see the options

The log.txt file will contain the log of the simulation with
the simulation states and events if was enabled. In the file
stats.txt the stats of the simulation will be printed. Moreover 
any plots will be saved in the same directory with *.png name.
